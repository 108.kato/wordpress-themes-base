export function mainVisualSlider() {
    var men_slide_counter = 0
        , women_slide_counter = 0
        , $men_elm = $("#js-slide-men")
        , $women_elm = $("#js-slide-women")
        , men_image_data = $men_elm.data('mvimg')
        , women_image_data = $women_elm.data('mvimg')
        , slide_timer = 5000
        ;

    $men_elm.empty();
    $women_elm.empty();
    for (var i = 0; i < men_image_data.length; i++) {
        $men_elm.append('<div class="screen" style="background-image:url(./assets/img/' + men_image_data[i] + ');"><\/div>');
        // if (i === 0) $men_elm.find(".screen").eq(0).addClass("active");
        $men_elm.find(".screen").removeClass("active").eq(men_slide_counter).addClass("active");
    }
    for (var i = 0; i < women_image_data.length; i++) {
        $women_elm.append('<div class="screen" style="background-image:url(./assets/img/' + women_image_data[i] + ');"><\/div>');
        // if (i === 0) $women_elm.find(".screen").eq(0).addClass("active");
        $women_elm.find(".screen").removeClass("active").eq(women_slide_counter).addClass("active");
    }
    setInterval(function () {
        men_slide_counter++;
        if (men_slide_counter === men_image_data.length) men_slide_counter = 0;
        $men_elm.find(".screen").removeClass("active").eq(men_slide_counter).addClass("active");

        women_slide_counter++;
        if (women_slide_counter === women_image_data.length) women_slide_counter = 0;
        $women_elm.find(".screen").removeClass("active").eq(women_slide_counter).addClass("active");

    }, slide_timer);
}