// import Swiper from 'swiper';
// var aboutslider;
export function youtube() {
    (function (win, doc) {
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        // プレイヤーを埋め込む場所(IDを指定)
        var ytArea = 'js-movie';
        // 埋め込むYouTube動画のID
        var ytID = "V0rGiqhKvdo";
        // プレイヤーの埋め込み
        var ytPlayer;
        function onYouTubeIframeAPIReady() {
            ytPlayer = new YT.Player(ytArea, {
                videoId: ytID,
                playerVars: {
                    vq: 'hd1080',
                    playsinline: 1,
                    autoplay: 1, // 自動再生
                    loop: 1, // ループ有効
                    controls: 0, // コントロールバー非表示
                    enablejsapi: 1, //JavaScript API 有効
                    iv_load_policy: 3, //動画アノテーションを表示しない
                    disablekb: 1, //キーボード操作OFF
                    showinfo: 0, //動画の再生が始まる前に動画のタイトルなどの情報を表示しない
                    rel: 0, //再生終了時に関連動画を表示しない
                    start: 1,
                    end: 51,
                },
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange,
                    'onPlaybackQualityChange': onPlayerPlaybackQualityChange,
                    'onError': onPlayerError
                }
            });
        }
        // プレーヤーの準備ができたとき
        function onPlayerReady(event) {
            // 動画をミュートにする
            var player = event.target;
            player.setPlaybackQuality('hd1080');
            player.mute();
            player.playVideo();
        }
        function onPlayerPlaybackQualityChange(event) {
            // console.log(event);
        }
        // onStateChangeのコールバック関数
        function onPlayerStateChange(event) {
            var status = event.data;
            var names = {
                '-1': '未開始',
                '0': '終了',
                '1': '再生中',
                '2': '停止',
                '3': 'バッファリング中',
                '5': '頭出し済み'
            };
            if (status == 0) {
                // console.log(ytPlayer.isMuted());
                // if (ytPlayer.isMuted()) {
                //     ytPlayer.mute();
                // } else {
                //     ytPlayer.unMute();
                // }
                ytPlayer.playVideo();
            }
        }
        // errorの発生時
        function onPlayerError(event) {
            var errorstatus = event.data;
            //何らかのエラーステータスが渡された場合、youtubeプレイヤーを削除する
            if (errorstatus !== '') {
                // $("#js-movie").remove();
            }
        }
        win.onYouTubeIframeAPIReady = onYouTubeIframeAPIReady;
    })(this, document);
}