import Swiper from 'swiper';
var UAParser = require("ua-parser-js");
var aboutslider
    , productslider
    , modalSliderThumb
    , modalSliderMain
    ;

var base_pathname = location.pathname;
export function modal() {
    var parser = new UAParser()
        , result = parser.getResult()
        ;

    // var base_pathname = location.pathname;

    aboutslider = new Swiper('#js-about-slider', {
        speed: 1000,
        loop: true,
        effect: "fade",
        autoHeight: true
    });
    $("#js-about-tab li").on("click", function () {
        var index = $("#js-about-tab li").index(this);
        // console.log(index);
        // console.log(aboutslider.realIndex);
        if (!$(this).hasClass("disabled")) {
            if (index !== aboutslider.realIndex) {
                // console.log(index);
                aboutslider.slideTo(index + 1);
            }
        }
    });
    var $about_1_img = $(".slide-1 .slider-men .img, .slide-1 .slider-women .img")
        , $about_2_img = $(".slide-2 .slider-men .img, .slide-2 .slider-women .img")
        , $about_3_img = $(".slide-3 .slider-men .img, .slide-3 .slider-women .img")
    ;
    aboutslider.on('slideNextTransitionStart', function () {
        $("#js-about-tab li").removeClass("current").eq(this.realIndex).addClass("current");
        // console.log('slideNextTransitionStart');
        // console.log(this.realIndex);
        if (this.realIndex === 0) {
            $about_1_img.css({
                "transform": "translateX(100px)",
                "opacity": 0
            });
            $about_2_img.css({
                "transform": "translateX(-100px)",
                "opacity": 0
            });
            $about_3_img.css({
                "transform": "translateX(-100px)",
                "opacity": 0
            });
        }
        if (this.realIndex === 1) {
            $about_2_img.css({
                "transform": "translateX(100px)",
                "opacity": 0
            });
            $about_1_img.css({
                "transform": "translateX(-100px)",
                "opacity": 0
            });
            $about_3_img.css({
                "transform": "translateX(-100px)",
                "opacity": 0
            });
        }
        if (this.realIndex === 2) {
            $about_3_img.css({
                "transform": "translateX(100px)",
                "opacity": 0
            });
            $about_1_img.css({
                "transform": "translateX(-100px)",
                "opacity": 0
            });
            $about_2_img.css({
                "transform": "translateX(-100px)",
                "opacity": 0
            });
        }
    });
    aboutslider.on('slideNextTransitionEnd', function () {
        // console.log('slideNextTransitionEnd');
        if (this.realIndex === 0) {
            $about_1_img.css({
                "transform": "translateX(0)",
                "opacity": 1
            });
            $about_2_img.css({
                "opacity": 0
            });
            $about_3_img.css({
                "opacity": 0
            });
        }
        if (this.realIndex === 1) {
            $about_2_img.css({
                "transform": "translateX(0)",
                "opacity": 1
            });
            $about_1_img.css({
                "opacity": 0
            });
            $about_3_img.css({
                "opacity": 0
            });
        }
        if (this.realIndex === 2) {
            $about_3_img.css({
                "transform": "translateX(0)",
                "opacity": 1
            });
            $about_1_img.css({
                "opacity": 0
            });
            $about_2_img.css({
                "opacity": 0
            });
        }
    });
    aboutslider.on('slidePrevTransitionStart', function () {
        $("#js-about-tab li").removeClass("current").eq(this.realIndex).addClass("current");
        if (this.realIndex === 0) {
            $about_1_img.css({
                "transform": "translateX(-100px)",
                "opacity": 0
            });
            $about_2_img.css({
                "transform": "translateX(100px)",
                "opacity": 0
            });
            $about_3_img.css({
                "transform": "translateX(100px)",
                "opacity": 0
            });
        }
        if (this.realIndex === 1) {
            $about_2_img.css({
                "transform": "translateX(-100px)",
                "opacity": 0
            });
            $about_1_img.css({
                "transform": "translateX(100px)",
                "opacity": 0
            });
            $about_3_img.css({
                "transform": "translateX(100px)",
                "opacity": 0
            });
        }
        if (this.realIndex === 2) {
            $about_3_img.css({
                "transform": "translateX(-100px)",
                "opacity": 0
            });
            $about_1_img.css({
                "transform": "translateX(100px)",
                "opacity": 0
            });
            $about_2_img.css({
                "transform": "translateX(100px)",
                "opacity": 0
            });
        }
    });
    aboutslider.on('slidePrevTransitionEnd', function () {
        // console.log('slidePrevTransitionEnd');
        if (this.realIndex === 0) {
            $about_1_img.css({
                "transform": "translateX(0)",
                "opacity": 1
            });
            $about_2_img.css({
                "opacity": 0
            });
            $about_3_img.css({
                "opacity": 0
            });
        }
        if (this.realIndex === 1) {
            $about_2_img.css({
                "transform": "translateX(0)",
                "opacity": 1
            });
            $about_1_img.css({
                "opacity": 0
            });
            $about_3_img.css({
                "opacity": 0
            });
        }
        if (this.realIndex === 2) {
            $about_3_img.css({
                "transform": "translateX(0)",
                "opacity": 1
            });
            $about_1_img.css({
                "opacity": 0
            });
            $about_2_img.css({
                "opacity": 0
            });
        }
    });

    $(document).on("click", "#js-slide-left", function () {
        aboutslider.slidePrev();
    });
    $(document).on("click", "#js-slide-right", function () {
        aboutslider.slideNext();
    });

    if (result.device.type === "mobile") {
        productslider = new Swiper('.product-img', {
            pagination: {
                el: '.swiper-pagination',
            },
        });
    }

    $(".look-box").on("click", function () {
        if ($(this).hasClass("look-mens")) {
            modal_indicate("men");
        } else {
            modal_indicate("women");
        }
    });

    // function modalSliderInit() {
        modalSliderThumb = new Swiper('#js-modal-slider-thumb', {
            spaceBetween: 1,
            slidesPerView: 18,
            loop: true,
            freeMode: true,
            loopedSlides: 84, //looped slides should be the same
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            centeredSlides: true,
            observer: true,
            centerInsufficientSlides: true,
            // observeSlideChildren: true,
            breakpoints: {
                768: {
                    slidesPerView: 5.2
                }
            },
            on: {
                init: function () {
                    // setTimeout(function () {
                    //     $("#js-modal-slider-thumb").addClass("on");
                    // }, 1000);
                    // setTimeout(function () {
                    //     $("#js-modal-slider-main").addClass("on");
                    // }, 800);
                },
            }
        });
        modalSliderMain = new Swiper('#js-modal-slider-main', {
            spaceBetween: 1,
            slidesPerView: 1.5,
            loop: true,
            loopedSlides: 84, //looped slides should be the same
            centeredSlides: true,
            observer: true,
            centerInsufficientSlides: true,
            simulateTouch: false,
            // observeSlideChildren: true,
            thumbs: {
                swiper: modalSliderThumb,
            },
            breakpoints: {
                768: {
                    slidesPerView: 1.2
                }
            },
            on: {
                // init: function () {
                //     setTimeout(function () {
                //         $("#js-modal-slider-main").addClass("on");
                //     }, 800);
                // },
            }
        });

        // modalSliderMain.on("slideChange", function () {
        //     console.log("realIndex = " + (this.realIndex));
        // });
    // }
    $(document).on("click", "#js-look-left", function () {
        modalSliderMain.slidePrev();
        // modalSliderMain.slideTo(currentIndex - 1);
    });
    $(document).on("click", "#js-look-right", function () {
        modalSliderMain.slideNext();
        // console.log(currentIndex);
        // modalSliderMain.slideTo(currentIndex + 1);
    });
    // $(document).on("click", ".swiper-slide-next", function () {
    //     modalSliderMain.slideNext();
    //     // modalSliderMain.slideTo(currentIndex + 1);
    // });
    // $(document).on("click", ".swiper-slide-prev", function () {
    //     modalSliderMain.slidePrev();
    //     // modalSliderMain.slideTo(currentIndex - 1);
    // });

    $(document).on("click", "#js-modal-wrap .swiper-slide-active .img, #js-modal-wrap .swiper-slide-duplicate-active .img", function () {
        if (result.device.type !== "mobile") {
            modal_close();
            // $(".credit-display-area").removeClass("credit-show");
        } else {
            var text = $("#js-modal-wrap .swiper-slide-active .credit-box, #js-modal-wrap .swiper-slide-duplicate-active .credit-box").prop("outerHTML");
            $(this).parent().parent().parent().parent().find(".credit-display-area").empty().append(text).addClass("credit-show");
        }
    });

    $(document).on("click", ".swiper-slide-active .modal-close, .swiper-slide-duplicate-active .modal-close", function () {
        modal_close();
    });
    $(document).on("click", ".modal-close.sp", function () {
        modal_close();
    });
    $(document).on("click", "#js-modal-wrap .btn-credit-show", function () {
        var text = $("#js-modal-wrap .swiper-slide-active .credit-box, #js-modal-wrap .swiper-slide-duplicate-active .credit-box").prop("outerHTML");
        $(this).parent().parent().find(".credit-display-area").empty().append(text).addClass("credit-show");
    });
    $(document).on("click", ".credit-display-area .modal-close", function () {
        $(".credit-display-area").removeClass("credit-show");
    });

    $(".look-box").on("click", function () {
        if ($(this).hasClass("look-mens")) {
            modal_indicate("men");
        } else {
            modal_indicate("women");
        }
    });
    // $(window).on("load", function () {
    //     modal_init();
    // });
    // function modal_init() {
    //     var modal_elm = '<div id="js-modal-wrap" class="md-modal-wrap"><div id="js-modal-slider-main" class="swiper-container look-top" ><ul class="swiper-wrapper"></ul><div id="js-look-left" class="md-look-slide-left"><span><img src="./assets/img/btn-about-left.png" alt="Prev" width="15"></span></div><div id="js-look-right" class="md-look-slide-right"><span><img src="./assets/img/btn-about-right.png" alt="Next" width="15"></span></div><div class="modal-close sp"></div><div class="btn-credit-show"><span>CREDIT</span></div></div><div id="js-modal-slider-thumb" class="swiper-container look-thumbs"><ul class="swiper-wrapper"></ul></div ><div class="credit-display-area"></div></div >';

    //     $("body").append(modal_elm);

    //     // if (getParam("style")) {
    //     //     modal_indicate("style-" + getParam("style"));
    //     // }
    // }
    function modal_close() {
        $("#js-modal-wrap").removeClass("is-show");
        // modalSliderThumb.destroy(true, true);
        // modalSliderMain.destroy(true, true);
        $(".credit-display-area").removeClass("credit-show");
        $("#js-modal-slider-main .swiper-wrapper").empty();
        $("#js-modal-slider-thumb .swiper-wrapper").empty();
        // history.pushState(null, null, base_pathname);
    };
    function modal_indicate(credit) {
        var thisCredit = credit
            , creditFile
            , creditMen = "./assets/json/look-men.json"
            , creditWomen = "./assets/json/look-women.json"
            ;

        if (credit === "men") {
            creditFile = creditMen;
        } else if (credit === "women") {
            creditFile = creditWomen;
        }

        var style_num = thisCredit.split("-")[1];
        // history.pushState(null, null, base_pathname + "?style=" + style_num);

        $.ajax({
            type: 'GET',
            url: creditFile,
            dataType: 'json',
            timeout: 20000,
            success: function (data) {

                for (var i = 0; i < data.length; i++) {
                    var lookimage = data[i]["lookimage"]
                        , category = data[i]["category"]
                        , numb = data[i]["numb"]
                        , model = data[i]["model"]
                        ;

                    var sliderElm = '<li class="swiper-slide"><div class="img" style="background-image:url(' + lookimage + ');"><img src="' + lookimage + '" alt=""></div><div class="credit-box">';
                    sliderElm += '<div class="cat">' + category + '</div><div class="numb">' + numb + '</div><div class="look-list">';

                    for (var j = 0; j < data[i]["items"].length; j++) {
                        var itemname = data[i]["items"][j]["item-name"]
                            , itemprice = data[i]["items"][j]["item-price"]
                            , itemlink = data[i]["items"][j]["item-link"]
                            , avarable = data[i]["items"][j]["avarable"]
                            , arrival = data[i]["items"][j]["arrival"]
                            ;

                        if (avarable) {
                            sliderElm += '<div class="credit-row"><div class="item-name">' + itemname + '</div><div class="item-price">' + itemprice + ' + tax</div><div class="store-link"><a href="' + itemlink + '" target="_blank" rel="noopener">ONLINE STORE</a></div></div>';
                        } else {
                            sliderElm += '<div class="credit-row"><div class="item-name">' + itemname + '</div><div class="item-price">' + arrival + '</div></div>';
                        }
                    }

                    sliderElm += '</div><div class="model">MODEL：' + model + '</div><div class="modal-close"></div></div>';

                    modalSliderMain.appendSlide(sliderElm);
                    // $("#js-modal-slider-main .swiper-wrapper").append(sliderElm);


                    var sliderThumbs = '<li class="swiper-slide" style="background-image: url(' + lookimage + ')"><img src="' + lookimage + '" alt=""></li>';

                    modalSliderThumb.appendSlide(sliderThumbs);

                    // $("#js-modal-slider-thumb .swiper-wrapper").append(sliderThumbs);
                }

                $("#js-modal-wrap").addClass("is-show");

                var total = parseInt(data.length);
                setTimeout(function () {
                    modalSliderThumb.slideTo(total);
                    $("#js-modal-slider-thumb").addClass("on");
                }, 1000);
                setTimeout(function () {
                    modalSliderMain.slideTo(total);
                    $("#js-modal-slider-main").addClass("on");
                }, 800);

            },
            error: function () {
                // alert('データの取得に問題が発生しました。お手数ですが、再度お試し下さい。');
            }
        });
    }
}