"use strict";
const { series, parallel, watch } = require('gulp');
// const del = require("del");
const { browserSyncInit, browserSyncReload } = require("./browsersync");
const { css } = require("./sass");
const { webpackScript } = require("./webpack_script");

// Clean assets
// function clean() {
//     return del(["./dist/assets/img/"]);
// }

function watchFiles(cb) {
    watch("_src/js/**/*.js", series(webpackScript, browserSyncReload));
    watch("_src/scss/**/*.scss", series(css, browserSyncReload));
    watch("./**/*.php").on("change", browserSyncReload);
    cb();
}

exports.watchFiles = watchFiles;
exports.default = series(parallel(webpackScript, css), watchFiles, browserSyncInit);