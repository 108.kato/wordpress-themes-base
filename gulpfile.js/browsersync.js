"use strict";
const browserSync = require("browser-sync").create();

function browserSyncInit(done) {
    browserSync.init({
        proxy: "" // vccw network setting URL
    });
    done();
}

function browserSyncReload(done) {
    browserSync.reload();
}
exports.browserSyncInit = browserSyncInit;
exports.browserSyncReload = browserSyncReload;